# -*- coding: utf-8 -*-
import numpy as np
#_________________________________Initialisation and constants value________________________________
from constants_lx_rk1 import Tfc,gamma_v,gamma_l,M_H2O,rho_H2O,Csat,Kshape

#_______________________________________Transitory functions_______________________________________

def lambda_eq(a_w):
    return 0.5*( 0.043 +17.81*a_w -39.85*a_w**2 +36.0*a_w**3 )*( 1 -np.tanh(100*(a_w -1)) ) \
         + 0.5*( 14 +8*( 1-np.exp(-Kshape*(a_w -1)) ) )*( 1+np.tanh(100*(a_w -1)) )
        
def a_w(Cw):
    return Cw/Csat

def Cw(s,C):
    return C+s*rho_H2O/M_H2O

def gamma(a_w):
    ome = omega(a_w)
    if a_w <=1 :
        return gamma_v
    else:
        return (1-ome)*gamma_v + ome*gamma_l

def omega(a_w):
    lambdaEq = lambda_eq(a_w)
    return (lambdaEq-14)/(22-14)*0.5 + (a_w -1)/(3 -1)*0.5

def D(lambdaa): #the diffusion coefficient of water in the membrane
    if lambdaa <= 3:
        return 3.1e-7*lambdaa*(np.exp(0.28*lambdaa)-1)*np.exp(-2346/Tfc)
    else:
        return 4.17e-8*lambdaa*(161*np.exp(-lambdaa)+1)*np.exp(-2346/Tfc)
    
def Q(s):
    return -(14735*s**5-300*s**4+9439*s**3+18878*s**2+75512*s-151024) / (17500*np.sqrt(1-s))