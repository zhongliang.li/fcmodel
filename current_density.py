# -*- coding: utf-8 -*-
import numpy as np
#_______________________________________Current density function______________________________________

#The current density raises in ~5s from 0.0 to 0.4 A.m-2 at tf/2.

def current_density(n,delta_t):
    
    i_fc = np.zeros(n)
    c = round(n/2) #The current density value changes at half-time.
    tlim=c*delta_t #time at which the current density raises.
    tau_c=1 #In 3*tau_c, 95% of the final value is reached.

    for j in range(c):
        i_fc[j]=0.0 #A.m-2. Start at 0.0 A.m-2
        
    for j in range(c,n):
        t=j*delta_t #current time.
        i_fc[j]=0.0+0.4*(1-np.exp( -(t-tlim)/tau_c )) #A.m-2. 
        
    return i_fc