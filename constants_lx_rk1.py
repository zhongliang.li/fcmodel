# -*- coding: utf-8 -*-
#________________________________Constant values used by Ling Xu_________________________________

#Algorithm constants
delta_t = 3e-2 #s. It is the time step of the algorithm.
tf = 500 #s. It is the simulation time.

#Specific constants of the problem
Tfc = 343 #K. It is the temperature of the fuel cell.
Pfc = 101325 #Pa. It is the pressure of the fuel cell.
k = 3 #. It is the ratio of the inlet molar flow of O2 over the reaction molar flow of O2.
Re = 1e-5 #ohm.m². It is the electron conduction resistance of the circuit.

#The physical characteristics of the PEMFC
    #Gas channel
Lch = 1.298 #m. It is the length of the channel.
Hgc= 1e-3 #m. It is the thickness of the gas channel.
    #Gas diffusion layer
Hgdl= 2.1e-4 #m. It is the thickness of the gas diffusion layer.
epsilon_gdl  = 0.6 #It is the anode/cathode GDL porosity.
    #Catalyst layer
Hcl = 1e-5 #m. It is the thickness of the anode or cathode catalyst layer.
epsilon_cl = 0.6 #It is the porosity of the catalyst layer, without units.
i0_c = 0.1 #A.m-2.It is the exchange current density at the cathode.
s_lim = 0.3 #It is the limit liquid saturation. 
    #Membrane
Hmem= 2.5e-5 #m. It is the thickness of the membrane.
rho_mem = 1980 #kg.m-3. It is the density of the dry membrane.
M_eq = 1.1e0 #kg.mol-1. It is the equivalent molar mass of ionomer.

#The constants based on the interaction between the structure and the flow
    #Vapor
gamma_v = 1.3 #s-1. It is the sorption/desorption rate of vapor at the ionomer/CL interface.
Dvc = 2.236e-5 #m².s-1. It is the vapor diffusivity in cathode.
Dva = 5.457e-5 #m².s-1. It is the vapor diffusivity in anode.
h_mv = 0.08 #m.s-1. It is the convective mass transfer coefficient of vapor.
    #Liquid water
gamma_l = 100 #s-1. It is the sorption/desorption rate of liquid at the ionomer/CL interface.
Kc = 6.875e-13 #m². It is the anode/cathode GDL permeability for liquid water.
theta_c = 110 #°. It is the contact angle of GDL for liquid water. 
    #Dioxygen
D_O2 = 2.9e-5 #m².s-1. It is the O2 diffusivity in cathode.
h_O2 = 0.078 #m.s-1. It is the convective mass transfer coefficient of O2. 

#The physical constants
F = 96485 #C.mol-1. It is the Faraday constant.
R = 8.314 #J. mol-1.K-1. It is the universal gas constant.
M_H2O = 0.018 #kg.mol-1. It is the water molecular mass.
rho_H2O = 1000 #kg.m-3. It is the water density. 
nu_l = 3.7e-7 #m².s-1. It is the liquid water kinematic viscosity. 
mu_l = 3.56e-4 #Pa.s. It is the liquid water dynamic viscosity. 
sigma = 0.0625 #N.m-1. It is the water surface tension. 
Csat = 10.9 #mol.m-3. It is the saturated vapor concentration at 70°C for a perfect gas.
Psat = 31200 #Pa. It is the saturated partial pressure of vapor at 70°C.
C_O2ref = 40.88 #mol.m-3. It is the reference concentration of oxygen.
y_O2 = 0.2095 #. It is the molar fraction of O2 in dry air.

#Mathematical factors
Kshape = 5 #It is a shape mathematical factor, without physical sense.

#Not understood yet
mu_cg = 1.881e-5 #Pa.s. It is the cathode gas dynamic viscosity. 
alpha_c = 0.5 #It is the transfer coefficient of the cathode.